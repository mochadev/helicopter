var $video = document.getElementById("videoGa");
var $videoSwitch = 0;

(function($){
    $(function() {
        $("body").on("click", "#videoGa", function(){
          // alert("aa");
          if ($videoSwitch == 0) {
            // alert("オン");
            document.getElementById("video001").play();
            document.getElementById("video001_pc").play();
            document.getElementById("h3001").style.display = "none";
            document.getElementById("p001").style.display = "none";
            document.getElementById("back001").style.opacity = 0;
            document.getElementById("video001").style.opacity = 1;
            document.getElementById("video001_pc").style.opacity = 1;
            $videoSwitch = 1;
          }else{
            // alert("オフ");
            document.getElementById("video001").pause();
            document.getElementById("video001_pc").pause();
            document.getElementById("h3001").style.display = "block";
            document.getElementById("p001").style.display = "block";
            document.getElementById("back001").style.opacity = 1;
            // document.getElementById("video001").style.opacity = 0.8;
            document.getElementById("video001_pc").style.opacity = 0.8;
            $videoSwitch = 0;
          }
        });
    });
})(jQuery);
