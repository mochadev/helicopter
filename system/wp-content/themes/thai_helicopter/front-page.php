<?php
/**
 * Template Name: フロントページ
 */

get_header();
?>
<div id="head">
	<div class="video_txt topLogoPosition">
		<img src="/images/logoWhite.png" alt="Thai Helicopter Service｜タイのプライベートヘリコプターサービス">
	</div>
<?php
if (!isMobile()) :
?>
	<video src="http://thai-helicopter.com/system/wp-content/uploads/2016/10/THS_Top_1025.mp4" loop="" autoplay="" class="pc">
	</video>
<?php
endif;
?>
	<?php echo do_shortcode('[gallery gallery_id="319" class="sp"]'); ?>
</div>
<?php
if (have_posts()) {
	the_post();
	the_content();
}

get_footer();
