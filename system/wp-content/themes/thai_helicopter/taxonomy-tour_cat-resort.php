<?php
/**
 * ツアー リゾート一覧表示
 */

global $term;
$term = getCurrentTerm();

get_header(); ?>

<?php get_template_part('template/tour_cat_head'); ?>

<div id="resorts">
	<p class="intro"><?php theFieldLang('catch_copy', 'tour_cat_'. $term->term_id); ?></p>
	<div class="line"></div>
<?php
if (have_posts()) :
	while (have_posts()) :
		the_post();

		// アイキャッチ画像を取得
		$image_url = get_bloginfo('template_directory'). '/images/thumbnail.png';
		if (has_post_thumbnail()) {
			$image_id = get_post_thumbnail_id();
			$image_src = wp_get_attachment_image_src($image_id, true);
			if (isset($image_src[0])) {
				$image_url = $image_src[0];
			}
		}
?>

	<a href="<?php echo getPermalink($post); ?>">
		<div class="scale">
			<img src="<?php echo $image_url; ?>" class="back">
			<div class="tt">
				<img src="<?php echo toTourNameImagePath($post); ?>" alt="<?php the_title(); ?>" style="min-height:0;">
			</div>
			<div class="mask">
				<div class="caption">
					<!-- <p class="spot">Pattaya</p> -->
					<p class="spotIntro"><?php the_field('list_excerpt'); ?></p>
				</div>
			</div>
		</div>
	</a>
<?php
	endwhile;
else:
?>
	<p>Comming soon!</p>
<?php
endif;
?>
</div>

<?php get_footer();
