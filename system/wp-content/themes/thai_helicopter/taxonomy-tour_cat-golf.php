<?php
/**
 * ツアー ゴルフ一覧表示
 */

global $term;
$term = getCurrentTerm();

$langText = new LangText();
$package001 = $langText->outputText('TOUR_SINGLE_05');
$package002 = $langText->outputText('TOUR_SINGLE_06');
$minute = $langText->outputText('min');
get_header(); ?>

<?php get_template_part('template/tour_cat_head'); ?>

<div id="resorts">
	<p class="intro"><?php theFieldLang('catch_copy', 'tour_cat_'. $term->term_id); ?></p>
	<div class="line"></div>
<?php
if (have_posts()) :
	$cnt = 0;
	while (have_posts()) :
		the_post();
		$cnt++;

		// アイキャッチ画像を取得
		$image_url = get_bloginfo('template_directory'). '/images/thumbnail.png';
		if (has_post_thumbnail()) {
			$image_id = get_post_thumbnail_id();
			$image_src = wp_get_attachment_image_src($image_id, true);
			if (isset($image_src[0])) {
				$image_url = $image_src[0];
			}
		}

		// HTML形成
		$title = get_the_title();
		$img_html =<<<EOM
		<div class="imgCellGolf">
			<img src="{$image_url}" alt="{$title}">
		</div>
EOM;
		$txt_html =<<<EOM
		<div class="txtCellGolf">
			<ul>
EOM;
		$txt_html .= '<li>' . get_field('place'). '</li>';
		// $txt_html .= '<li class="stars">';
		// for ($i=1; $i<=get_field('estimate'); $i++) { $txt_html .='★'; }
		$txt_html .=
			'<li><p>'. get_field('time'). $minute .'</p></li>'
			.'<li><p style="margin-bottom:5px;">' . get_field('price') . '</p><div class="priceInfo golfP">
					<p>Flight Fee<br><span class="moreSmall">' . $package001. '</span></p>
				</div>
				<div class="batu">
					<p>+</p>
				</div>
				<div class="priceInfo golfP">
					<p>Landing Fee<br><span class="moreSmall">' . $package002. '</span></p>
				</div>'
			.'<li class="golfCot">' . theContentLangText(). '</li>'
			.'</ul>'
			.'</div>'
			;
?>

	<div class="cellGolf" id="golf_<?php echo $post->ID; ?>">
<?php
		// if (($cnt % 2) === 1) {
		// 	echo $img_html. $txt_html;
		// } else {
		// 	echo $txt_html. $img_html;
		// }
		echo $img_html. $txt_html;
?>
	</div>

<?php
	endwhile;
else:
?>
	<p>Comming soon!</p>
<?php
endif;
?>
	<div class="center">
		<a href="/contact/">
			<div class="btn red">
				Contact
			</div>
		</a>
	</div>
</div>

<?php get_footer();
