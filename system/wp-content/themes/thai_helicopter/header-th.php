<?php
/**
 * ヘッダー表示
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,user-scalable=no,maximum-scale=1" />
	<title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/css/default.css">
	<link rel="stylesheet" type="text/css" href="/css/lily.css">
	<link rel="stylesheet" type="text/css" href="/css/common.css">
	<link rel="stylesheet" type="text/css" href="/css/flights.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" media="all" type="text/css" href="/css/response.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

	<!-- JavaScript -->
	<script src="/js/jquery-2.2.4.min.js"></script>

	<!-- bxSlider -->
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/lib/jquery.bxslider/jquery.bxslider.css">
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bxslider.js"></script>

	<!-- slick -->
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/lib/jquery.slick/slick.css" />
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/lib/jquery.slick/slick.min.js"></script>

	<!-- WordPressカスタマイズCSS -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css">

	<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-85108451-1', 'auto');
 ga('send', 'pageview');

</script>
	<?php wp_head(); ?>
</head>
<body>
<nav id="nav" class="nav">
	<a href="/th/" class="nav_logo"><img src="/images/logo-color.png" class="headlogo"></a>
	<div class="nav_header">
		<ul>
			<!-- <a href="/th/charter/"><li>CHARTER</li></a>
			<li class="dropdown">
				TOUR
				<div class="dropdown-content" id="menu-dropdown">
<?php
$terms = get_terms('tour_cat', array('orderby' => 'ID', 'order' => 'ASC'));
foreach ($terms as $term) :
?>
					<a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
<?php
endforeach;
?>
				</div>
			</li>
			<!-- <a href="/th/member/"><li>MEMBER</li></a> -->
			<?php if(!is_front_page()): ?><!--Topページじゃないとき-->
				<a href="/th/#points"><li>CHARTER</li></a>
				<a href="/th/#gallery"><li>GALLERY</li></a>
				<a href="/th/#social"><li>SNS</li></a>
				<a href="/th/#team"><li>TEAM</li></a>
				<a href="/th/#aboutus"><li>ABOUTUS</li></a>
				<a href="/th/#contact"><li>CONTACT</li></a>
			<?php else: ?><!-- Topページのとき -->
				<a href="#points"><li>CHARTER</li></a>
				<a href="#gallery"><li>GALLERY</li></a>
				<a href="#social"><li>SNS</li></a>
				<a href="#team"><li>TEAM</li></a>
				<a href="#aboutus"><li>ABOUTUS</li></a>
				<a href="#contact"><li>CONTACT</li></a>
			<?php endif; ?>
			<li><span class="tel">TEL:</span>+66(0)2-571-3355</li>
		</ul>
	</div>
	<div class="lang" id="lang">
		<li><a href="/"><img src="/images/japan.png"/> JP</a></li>
		<li><a href="/th/"><img src="/images/thailand.png"/> TH</a></li>
		<li><a href="/en/"><img src="/images/uk.png"/> EN</a></li>
	</div>
</nav>
<nav id="responseMenu">
	<a href="/th/"><img src="/images/logo-color.png" class="headlogo"></a>
	<div class="hamburger">
		<i class="fa fa-bars" aria-hidden="true" id="barBtn"></i>
	</div>
	<div class="lang" id="lang">
		<a onclick="onclickLang()" class="langbtn"><img src="/images/thailand.png"/></a>
  	<div id="lang-dropdown" class="dropdown-content">
			<li><a href="/en/">English  <img class="lang-img" src="/images/uk.png"/></a></li>
			<li><a href="/th/">Thai  <img class="lang-img" src="/images/thailand.png"/></a></li>
			<li><a href="/">Japanese  <img class="lang-img" src="/images/japan.png"/></a></li>
  	</div>
	</div>
</nav>
<!-- <div id="hamburgerContent">
	<ul>
		<a href="/th/"><li>TOP</li></a>
		<a href="/th/charter/"><li>CHARTER</li></a>
		<a href="/th/tour/tour_cat/resort"><li>RESORT</li></a>
		<a href="/th/tour/tour_cat/sightseeing"><li>SIGHTSEEING</li></a>
		<a href="/th/tour/tour_cat/golf"><li>GOLF</li></a>
		<a href="/th/tour/tour_cat/casino"><li>CASINO</li></a>
		<a href="/th/member/"><li>MEMBER</li></a>
		<a href="/th/contact/"><li>CONTACT</li></a>
		<a href="tel:+66(0)2-138-9576"><li><i class="fa fa-phone" aria-hidden="true"></i>CALL</li></a>
</div> -->
<div id="hamburgerContent">
	<ul>
		<?php if(!is_front_page()): ?><!--Topページじゃないとき-->
			<a href="/th/#points"><li>CHARTER</li></a>
			<a href="/th/#gallery"><li>GALLERY</li></a>
			<a href="/th/#social"><li>SNS</li></a>
			<a href="/th/#team"><li>TEAM</li></a>
			<a href="/th/#aboutus"><li>ABOUTUS</li></a>
			<a href="/th/#contact"><li>CONTACT</li></a>
		<?php else: ?><!-- Topページのとき -->
			<a href="#points"><li>CHARTER</li></a>
			<a href="#gallery"><li>GALLERY</li></a>
			<a href="#social"><li>SNS</li></a>
			<a href="#team"><li>TEAM</li></a>
			<a href="#aboutus"><li>ABOUTUS</li></a>
			<a href="#contact"><li>CONTACT</li></a>
		<?php endif; ?>
		<a href="tel:+66(0)2-138-9576"><li><i class="fa fa-phone" aria-hidden="true"></i>CALL</li></a>
</div>
