<?php
global $term;

// 背景ギャラリーを取得
$gallery_object = get_field('gallery', $term->taxonomy. '_'. $term->term_id);
$gallery_id = $gallery_object->ID;
?>

<?php if (toPageHeadClass($term->slug) == "sightseeingBack"): ?>
	<div id="head" class="<?php echo toPageHeadClass($term->slug); ?>">
		<?php if (!empty($gallery_id)) { echo do_shortcode('[gallery gallery_id='. $gallery_id . ']'); } ?>
		<div class="video_txt floatleft">
			<div class="tourism_txt">
				<h1 class="halis_bold"><?php single_cat_title(); ?></h1>
				<p class="sightTxt"><?php theFieldLang('main_visual_text', 'tour_cat_'. $term->term_id); ?></p>
			</div>
			<div class="tourism_video">
				<section id="videoGa">
				<div class="videoWrap" id="back001">
					<div id="videoDesc">
						<h3 id="h3001">Sightseeing from sky</h3>
						<p id="p001"><i class="fa fa-play" aria-hidden="true"></i></p>
					</div>
				</div>
				<video src="http://thai-helicopter.com/system/wp-content/uploads/2016/10/Heliluck-Gallery-4.mp4" id="video001" class="pc"></video>
				<video src="http://thai-helicopter.com/system/wp-content/uploads/2016/10/Heliluck-Gallery-4.mp4" id="video001" class="sp" poster="/images/videoPosterSight.jpg"></video>
				</section>
			</div>
			<!-- <div id="spVideo">
				<video src="http://thai-helicopter.com/system/wp-content/uploads/2016/09/Heliluck-Gallery-4-1.mp4" id="video001" poster="/images/1.jpg" controls></video>
			</div> -->
		</div>
	</div>

<?php elseif (toPageHeadClass($term->slug) == "casinoBack"): ?>
	<div id="head" class="golCasiHeadHeight">
		<div class="casinofirstback" id="twoContent">
			<div class="twoText">
				<h1 class="halis_bold"><?php single_cat_title(); ?></h1>
				<p class="cT"><?php theFieldLang('main_visual_text', 'tour_cat_'. $term->term_id); ?></p>
			</div>
		</div>
		<div class="casinoBack" id="twoContent">
		</div>
	</div>
	<div class="clear"></div>
<?php elseif (toPageHeadClass($term->slug) == "golfBack"): ?>
	<div id="head" class="golCasiHeadHeight">
		<div class="golfBack" id="twoContent">
		</div>
		<div class="golffirstback" id="twoContent">
			<div class="twoText">
				<h1 class="halis_bold"><?php single_cat_title(); ?></h1>
				<p><?php theFieldLang('main_visual_text', 'tour_cat_'. $term->term_id); ?></p>
			</div>
		</div>
	</div>
	<div class="clear"></div>
<?php else: ?>
<div id="head" class="<?php echo toPageHeadClass($term->slug); ?>">
	<div class="pc">
		<?php if (!empty($gallery_id)) { echo do_shortcode('[gallery gallery_id='. $gallery_id . ']'); } ?>
	</div>
	<div class="sp sp-bx-wrapper">
		<?php if (!empty($gallery_id)) { echo do_shortcode('[gallery gallery_id=535]'); } ?>
	</div>
	<div class="video_txt">
		<h1 class="halis_bold"><?php single_cat_title(); ?></h1>
		<p><?php theFieldLang('main_visual_text', 'tour_cat_'. $term->term_id); ?></p>
	</div>
</div>
<?php endif; ?>
