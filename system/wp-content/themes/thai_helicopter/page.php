<?php
/**
 * 固定ページ表示
 */

get_header();

if (have_posts()) :
	the_post();

	$post_id = $post->ID;
	$post_name = $post->post_name;
	$post_array = array(1078,1080,67);

	// 親がある場合は親の投稿を取得
	if ($post->post_parent > 0) {
		$parent_post = get_post($post->post_parent);
		$post_id = $parent_post->ID;
		$post_name = $parent_post->post_name;
	}

	// 背景ギャラリーを取得
	$gallery_object = get_field('gallery', $post_id);
	$gallery_id = $gallery_object->ID;
	if ($post_name === 'top') {
		$post_name = 'contact';
	}
?>

<div id="head" class="<?php echo toPageHeadClass($post_name, array('has_slider' => !empty($gallery_id)));
// if($post_id == 1078 || $post_id == 1080 || $post_id == 67){
// 	echo " charterBright";
// }
if(in_array($post_id, $post_array)){
	echo " charterBright";
}
 ?>">
	<div class="pc">
		<?php if (!empty($gallery_id)) { echo do_shortcode('[gallery gallery_id='. $gallery_id . ']'); } ?>
	</div>
	<div class="sp sp-bx-wrapper">
		<?php if (!empty($gallery_id)) { echo do_shortcode('[gallery gallery_id=747]'); } ?>
	</div>

	<div class="video_txt">
		<?php if ($post_id == 67): ?>
			<h1 class="halis_bold"><?php the_title(); ?></h1>
			<p style="font-size: 1.2em;"><?php the_field('main_visual_text', $post_id); ?></p>
		<?php else: ?>
			<h1 class="halis_bold"><?php the_title(); ?></h1>
			<p><?php the_field('main_visual_text', $post_id); ?></p>
		<?php endif; ?>
	</div>
</div>

<?php
	the_content();
endif;

get_footer();
