<?php
/**
 * フッター表示
 */
?>

<footer>
	<div id="footer">
		<div class="links">
			<div class="col-fo-5">
				<img src="/images/logo-color.png">
				<!-- <h4>Office</h4>
				<p class="halisRegular">139/76, Pattanachonnabot, Khlongsongtonnun, Ladkrabang, Bangkok 10520</p> -->
				<h4>Helicopter Hangar</h4>
				<p class="halisRegular">99/99, Soi Rongmee, Bangbuathong-sainoi Rd., Bangbuathong, Nonthaburi, Thailand 11110</p>
				<h4>Phone/Fax</h4>
				<!-- <p class="halisRegular">+66(0)2-138-9576</p> -->
				<p class="halisRegular">+66(0)2-571-3355</p>
				<p class="halisRegular">（日本語はメールでの問い合わせをお願いします。）</p>
				<h4>Email</h4>
				<p class="halisRegular">info@thai-helicopter.com</p>
				<h4>Open hour</h4>
				<p class="halisRegular">Mon. - Sat.<br>8:00 am - 5:00 pm</p>
			</div>
			<!-- <div class="col-fo-1">
				<h3>RESORT</h3>
				<ul>
<?php
$args = array(
	'post_type' => 'tour',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'tour_cat',
			'terms' => 'resort',
			'field' => 'slug'
		)
	)
);
$query = new WP_Query($args);
if ($query->have_posts()) :
	while ($query->have_posts()) :
		$query->the_post();
?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php
	endwhile;
endif;
?>
				</ul>
			</div>
			<div class="col-fo-1">
				<h3>GOLF</h3>
				<ul>
<?php
$args = array(
	'post_type' => 'tour',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'tour_cat',
			'terms' => 'golf',
			'field' => 'slug'
		)
	)
);
$query = new WP_Query($args);
if ($query->have_posts()) :
	while ($query->have_posts()) :
		$query->the_post();
?>
					<li><a href="/tour/tour_cat/golf#golf_<?php echo $query->post->ID; ?>"><?php the_title(); ?></a></li>
<?php
	endwhile;
endif;
?>
				</ul>
			</div>
			<div class="col-fo-1">
				<h3>SIGHTSEEING</h3>
				<ul>
<?php
$args = array(
	'post_type' => 'tour',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'tour_cat',
			'terms' => 'sightseeing',
			'field' => 'slug'
		)
	)
);
$query = new WP_Query($args);
if ($query->have_posts()) :
	while ($query->have_posts()) :
		$query->the_post();
?>
					<li><a href="/tour/tour_cat/sightseeing#sightseeing_<?php echo $post->ID; ?>"><?php the_title(); ?></a></li>
<?php
	endwhile;
endif;
?>
				</ul>
			</div>
			<div class="col-fo-1">
				<h3>CASINO</h3>
				<ul>
<?php
$args = array(
	'post_type' => 'tour',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'tour_cat',
			'terms' => 'casino',
			'field' => 'slug'
		)
	)
);
$query = new WP_Query($args);
if ($query->have_posts()) :
	while ($query->have_posts()) :
		$query->the_post();
?>
					<li><a href="/tour/tour_cat/casino#casino_<?php echo $query->post->ID; ?>"><?php the_title(); ?></a></li>
<?php
	endwhile;
endif;
?>
				</ul>
			</div> -->
			<?php if(!is_front_page()): ?><!--Topページじゃないとき-->
				<div class="col-fo-1">
					<h3>CONTENTS</h3>
					<a href="/#points"><li>CHARTER</li></a>
					<a href="/#gallery"><li>GALLERY</li></a>
				</div>
				<div class="col-fo-1">
					<h3>OHTER</h3>
					<ul>
						<a href="/#social"><li>SNS</li></a>
						<a href="/#team"><li>TEAM</li></a>
						<a href="/#aboutus"><li>ABOUTUS</li></a>
						<a href="/#contact"><li>CONTACT</li></a>
					</ul>
				</div>
			<?php else: ?><!-- Topページのとき -->
				<div class="col-fo-1">
					<h3>CONTENTS</h3>
					<a href="#points"><li>CHARTER</li></a>
					<a href="#gallery"><li>GALLERY</li></a>
				</div>
				<div class="col-fo-1">
					<h3>OHTER</h3>
					<ul>
						<a href="#social"><li>SNS</li></a>
						<a href="#team"><li>TEAM</li></a>
						<a href="#aboutus"><li>ABOUTUS</li></a>
						<a href="#contact"><li>CONTACT</li></a>
					</ul>
				</div>
			<?php endif; ?>

		</div>
		<div class="social">
			<h3>Social Media</h3>
			<ul>
				<a href="https://www.instagram.com/heliluck/"><li class="insta"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></li></a>
				<a href="https://www.facebook.com/heliluck/"><li class="facebook"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></li></a>
				<a href="https://twitter.com/heliluck"><li class="twitter"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></li></a>
				<a href="https://plus.google.com/101668873471331388336"><li class="google"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></li></a>
				<a href="https://www.youtube.com/user/Heliluck"><li class="youtube"><i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></li></a>
				<a href=""><li class="tripAdvisor"><i class="fa fa-tripadvisor fa-2x" aria-hidden="true"></i></li></a>
		</div>
	</div>
</footer>
<!-- <div class="footerNav">
	<ul>
		<li class="foCon">Contact us</li>
		<li class="foTel">0120-000-0000</li>
	</ul>
	<div class="clear"></div>
</div> -->
<!-- <script type="text/javascript" src="/js/jquery.smoothScroll.js"></script> -->
<script type="text/javascript" src="/js/scroll.js"></script>
<script type="text/javascript" src="/js/sticky.js"></script>
<script type="text/javascript" src="/js/team.js"></script>
<script type="text/javascript" src="/js/video.js"></script>
<script type="text/javascript" src="/js/dropdown.js"></script>
<?php wp_footer(); ?>
</body>
</html>
