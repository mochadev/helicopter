<?php

if (is_admin()) {
	require_once dirname(__FILE__). '/functions/admin.php';
} else {
	require_once dirname(__FILE__). '/functions/front.php';
	require_once dirname(__FILE__). '/class/LangText.php';
}
add_theme_support('post-thumbnails');

// 【MW MW Form】エラーメッセージをカスタマイズ・・・日本語
function my_validation_rule( $Validation, $data, $Data ) {
	$Validation->set_rule( 'user_name', 'noEmpty', array( 'message' => '※必須項目を入力してください。' ) );
	$Validation->set_rule( 'tel', 'noEmpty', array( 'message' => '※必須項目を入力してください。' ) );
	$Validation->set_rule( 'email', 'noEmpty', array( 'message' => '※必須項目を入力してください。' ) );
	$Validation->set_rule( 'email', 'mail', array( 'message' => '※メールアドレスの形式ではありません。' ) );
	$Validation->set_rule( 'content', 'noEmpty', array( 'message' => '※必須項目を入力してください。' ) );
	return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-700', 'my_validation_rule', 10, 3 );

// 【MW MW Form】エラーメッセージをカスタマイズ・・・英語
function my_validation_rule_english( $Validation, $data, $Data ) {
	$Validation->set_rule( 'user_name', 'noEmpty', array( 'message' => 'Please fill in the box to proceed' ) );
	$Validation->set_rule( 'tel', 'noEmpty', array( 'message' => 'Please fill in the box to proceed' ) );
	$Validation->set_rule( 'email', 'noEmpty', array( 'message' => 'Please fill in the box to proceed' ) );
	$Validation->set_rule( 'email', 'mail', array( 'message' => 'Please write your email correctly.' ) );
	$Validation->set_rule( 'content', 'noEmpty', array( 'message' => 'Please fill in the box to proceed' ) );
	return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-1097', 'my_validation_rule_english', 10, 3 );

// 【MW MW Form】エラーメッセージをカスタマイズ・・・タイ語
function my_validation_rule_thai( $Validation, $data, $Data ) {
	$Validation->set_rule( 'user_name', 'noEmpty', array( 'message' => 'กรุณากรอกข้อมูลในช่องว่าง' ) );
	$Validation->set_rule( 'tel', 'noEmpty', array( 'message' => 'กรุณากรอกข้อมูลในช่องว่าง' ) );
	$Validation->set_rule( 'email', 'noEmpty', array( 'message' => 'กรุณากรอกข้อมูลในช่องว่าง' ) );
	$Validation->set_rule( 'email', 'mail', array( 'message' => 'กรุณากรอกอีเมล์ให้ถูกต้อ' ) );
	$Validation->set_rule( 'content', 'noEmpty', array( 'message' => 'กรุณากรอกข้อมูลในช่องว่าง' ) );
	return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-1099', 'my_validation_rule_thai', 10, 3 );
