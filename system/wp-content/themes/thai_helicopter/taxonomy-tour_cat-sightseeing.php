<?php
/**
 * ツアー TOURISM覧表示
 */

global $term;
$term = getCurrentTerm();

get_header(); ?>

<?php get_template_part('template/tour_cat_head'); ?>

<div id="resorts">
	<p class="intro"><?php theFieldLang('catch_copy', 'tour_cat_'. $term->term_id); ?></p>
	<div class="line"></div>

<?php
if (have_posts()) :
	while (have_posts()) :
		the_post();

		// アイキャッチ画像を取得
		$image_url = get_bloginfo('template_directory'). '/images/thumbnail.png';
		if (has_post_thumbnail()) {
			$image_id = get_post_thumbnail_id();
			$image_src = wp_get_attachment_image_src($image_id, true);
			if (isset($image_src[0])) {
				$image_url = $image_src[0];
			}
		}

		$casino_kind = get_field('casino_kind');
?>

	<div class="cellTourCasi" id="sightseeing_<?php echo $post->ID; ?>">
		<div class="tourcasiImg tour001" style="background-image: url(<?php echo $image_url; ?>);">
		  <!-- <img src="images/bangkok.png"> -->
		</div>
		<h2><?php echo the_title(); ?></h2>
		<ul class="tourcasiInfo">
			<li><?php echo number_format(get_field('price')); ?> THB<br><span class="small"><?php LangText::output('TOUR_SINGLE_01'); ?></span></li>
			<li><?php the_field('from_place'); ?><br><span class="small"><?php LangText::output('TOUR_SINGLE_02'); ?></span></li>
			<!-- <li><?php the_field('to_place'); ?><br><span class="small">目的地</span></li> -->
			<li><?php the_field('flight_time'); ?> <?php the_field('flight_time_unit'); ?><br><span class="small"><?php LangText::output('TOUR_SINGLE_04'); ?></span></li>
			</ul>
			<div class="priceInfo ssP">
					<p>Flight Fee<br><span class="moreSmall"><?php LangText::output('TOUR_SINGLE_05'); ?></span></p>
				</div>
				<div class="batu">
					<p>+</p>
				</div>
				<div class="priceInfo ssP">
					<p>Landing Fee<br><span class="moreSmall"><?php LangText::output('TOUR_SINGLE_06'); ?></span></p>
				</div>
		<?php theContentLang(); ?>
	</div>

<?php
	endwhile;
else:
?>
	<p>Comming soon!</p>
<?php
endif;
?>
	<div class="center">
		<a href="/contact/">
			<div class="btn red">
				Contact
			</div>
		</a>
	</div>
</div>

<?php get_footer();
