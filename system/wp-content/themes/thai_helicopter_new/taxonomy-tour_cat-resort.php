<?php
/*
 * ツアー リゾート一覧表示
 */

global $term;
$term = getCurrentTerm();

get_header(); ?>

<?php get_template_part('template/tour_cat_head'); ?>

<div id="resorts">
    <p class="intro"><?php theFieldLang('catch_copy', 'tour_cat_'. $term->term_id); ?></p>
    <div class="line"></div>
<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();

        // アイキャッチ画像を取得
        $image_url = get_bloginfo('template_directory'). '/images/thumbnail.png';
        if (has_post_thumbnail()) {
            $image_id = get_post_thumbnail_id();
            $image_src = wp_get_attachment_image_src($image_id, true);
            if (isset($image_src[0])) {
                $image_url = $image_src[0];
            }
        }
?>
    <div class="resortContainer">
        <a href="<?php echo getPermalink($post); ?>">
            <div class="resortContent">
                <h2><?php the_title(); ?></h2>
								<h3>Schedule</h3>
								<p>ここにフライト時間がきます</p>
								<h3>Price</h3>
                <p>ここにフライト金額がきます</p>
								<h3>About</h3>
                <p>ここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきますここにフライト説明文がきます</p>
            </div>
            <div class="scale">
                <img src="<?php echo $image_url; ?>" class="back">
                <div class="tt">
                    <img src="<?php echo toTourNameImagePath($post); ?>" alt="<?php the_title(); ?>" style="min-height:0;">
                </div>
                <div class="mask">
                    <div class="caption">
                        <!-- <p class="spot">Pattaya</p> -->
                        <p class="spotIntro"><?php the_field('list_excerpt'); ?></p>
                    </div>
                </div>
            </div>
        </a>
        </div>
<?php
    endwhile;
else:
?>
    <p>Comming soon!</p>
<?php
endif;
?>

</div>

<style>
.resortContainer {
    position: relative;
    height: 400px;
    padding-top: 20px;
}
.resortContent {
    width: 60%;
		height: 80%;
    color: black;
    display: inline-block;
}
.resortContent h2 {
	  text-align: left!important;
}
.resortContent h2 h3 p {
		padding-left: 20px;
}
.resortContent p {
		margin-bottom: 10px;
		width: 100%;
}
.resortContainer .scale {
		width: 30%;
		height: 80%;
		vertical-align: middle;
		position: absolute;
		right: 0;
}

</style>

<?php get_footer();
