<?php get_header(); ?>

<?php
if (have_posts()) :
	while (have_posts()) :
		the_post();
		// アイキャッチ画像を取得
		$image_url = get_bloginfo('template_directory'). '/images/thumbnail.png';
		if (has_post_thumbnail()) {
			$image_id = get_post_thumbnail_id();
			$image_src = wp_get_attachment_image_src($image_id, true);
			if (isset($image_src[0])) {
				$image_url = $image_src[0];
			}
		}
?>
<div id="head" class="detailBack" style="background: url(<?php echo $image_url; ?>);">
	<div class="video_txt">
		<img src="<?php the_field('title_img'); ?>" width="230px;">
		<p><?php the_field('main_visual_text'); ?></p>
	</div>
</div>
<div class="backColor">
	<div class="resortinfo">
		<!-- <h1><?php the_title(); ?></h1> -->
		<!-- <img src="<?php the_field('title_img'); ?>" width="230px;"> -->
		<p><?php the_field('list_excerpt'); ?></p>
		<ul class="tourcasiInfo resortPack">
			<li style="float:left; margin-left:0;"><?php echo number_format(get_field('price')); ?> THB<br><span class="small"><?php LangText::output('TOUR_SINGLE_01'); ?></span></li>
			<li style="float:left;"><?php the_field('from_place'); ?><br><span class="small"><?php LangText::output('TOUR_SINGLE_02'); ?></span></li>
			<li style="float:left;"><?php the_field('to_place'); ?><br><span class="small"><?php LangText::output('TOUR_SINGLE_03'); ?></span></li>
			<li style="float:left;"><?php the_field('flight_time'); ?> <?php LangText::theField('flight_time_unit'); ?><br><span class="small"><?php LangText::output('TOUR_SINGLE_04'); ?></span></li>
		</ul>
		<div class="clear"></div>
		<h2 class="planP">Package Plan</h2>
		<div class="priceInfo">
			<p>Flight Fee<br><span class="moreSmall"><?php LangText::output('TOUR_SINGLE_05'); ?></span></p>
		</div>
		<div class="batu">
			<p>+</p>
		</div>
		<div class="priceInfo">
			<p>Landing Fee<br><span class="moreSmall"><?php LangText::output('TOUR_SINGLE_06'); ?></span></p>
		</div>
		<div class="batu">
			<p>+</p>
		</div>
		<div class="priceInfo">
			<p>Waiting Fee<br><span class="moreSmall"><?php LangText::output('TOUR_SINGLE_07'); ?></span></p>
		</div>
		<!-- <p class="resortinfoTxt pattaya">価格については、<br>Flight Fee(往復)/Landing Fee(計4回の着陸)<br>/Waiting Fee(4時間まで無料、以降1時間3,000THB)<br>が含まれた値段になります。</p> -->
		<a href="/contact/">
			<div class="btn red pattayaBtn">
				Contact
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div id="detailContent" class="padding">
		<?php theContentLang(); ?>
	</div>
</div>
<?php
	endwhile;
else:
?>
	<p>Comming soon!</p>
<?php
endif;

get_footer();
