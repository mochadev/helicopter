<?php
/**
 * ツアー TOURISM覧表示
 */

$counter = 1;
global $term;
$term = getCurrentTerm();
$langText = new LangText();
$minute = $langText->outputText('min');

get_header(); ?>

<?php get_template_part('template/tour_cat_head'); ?>

<div id="resorts">
		<p class="intro"><?php theFieldLang('catch_copy', 'tour_cat_'. $term->term_id); ?></p>
		<div class="line"></div>
		<div class="sightseeingContainer">
		<?php
		if (have_posts()) :
				while (have_posts()) :
						the_post();
						// アイキャッチ画像を取得
						$image_url = get_bloginfo('template_directory'). '/images/thumbnail.png';
						if (has_post_thumbnail()) {
								$image_id = get_post_thumbnail_id();
								$image_src = wp_get_attachment_image_src($image_id, true);
								if (isset($image_src[0])) {
										$image_url = $image_src[0];
								}
						}
		?>
		<?php
						if($counter == 1) :
		?>
		<a href="<?php echo getPermalink($post); ?>">
			<div class="sightseeingImage">
				<img src="<?php echo $image_url; ?>" class="back">
				<h2 class="sightseeingTitle"><?php the_title(); ?></h2>
			</div>
			<div class="sightseeingDescription">
				<ul id="sightseeingDetail">
					<li>
						<p> - <?php echo get_field('flight_time')." ".get_field('flight_time_unit'); ?></p>
					</li>
					<li>
						<p> - <?php echo get_field('price'); ?> THB</p>
					</li>
				</ul>
				<div id="sightseeingText">
					<p><?php echo theContentLangText(); ?></p>
				</div>
			</div>
		</a>
		<?php
						elseif($counter == 2) :
		?>
		<a href="<?php echo getPermalink($post); ?>" style="padding-left: 20px; padding-right: 20px;">
			<div class="sightseeingImage">
				<img src="<?php echo $image_url; ?>" class="back">
				<h2 class="sightseeingTitle"><?php the_title(); ?></h2>
			</div>
			<div class="sightseeingDescription">
				<ul id="sightseeingDetail">
					<li>
						<p> - <?php echo get_field('flight_time')." ".get_field('flight_time_unit'); ?></p>
					</li>
					<li>
						<p> - <?php echo get_field('price'); ?> THB</p>
					</li>
				</ul>
				<div id="sightseeingText">
					<p><?php echo theContentLangText(); ?></p>
				</div>
			</div>
		</a>
		<?php
						elseif($counter == 3) :
		?>
		<a href="<?php echo getPermalink($post); ?>">
			<div class="sightseeingImage">
				<img src="<?php echo $image_url; ?>" class="back">
				<h2 class="sightseeingTitle"><?php the_title(); ?></h2>
			</div>
			<div class="sightseeingDescription">
				<ul id="sightseeingDetail">
					<li>
						<p> - <?php echo get_field('flight_time')." ".get_field('flight_time_unit'); ?></p>
					</li>
					<li>
						<p> - <?php echo get_field('price'); ?> THB</p>
					</li>
				</ul>
				<div id="sightseeingText">
					<p><?php echo theContentLangText(); ?></p>
				</div>
			</div>
		</a>
		<div class="clear"></div>
		<?php
						endif;
		?>
		<?php
				$counter = ($counter == 3) ? 1 : ($counter + 1);
				endwhile;
		else:
		?>
				<p>Comming soon!</p>
		<?php
		endif;
		?>



		</div>
</div>
<style>
</style>
<?php get_footer();
