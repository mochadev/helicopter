<?php
/**
 * 管理画面で使う共通処理
 */

/**
 * 管理画面に独自cssを読み込ませる
 */
function myAdminStyle() {
	wp_enqueue_style('myAdminStyle', get_template_directory_uri() .'/css/admin.css' );
}
add_action('admin_enqueue_scripts', 'myAdminStyle');

/**
 * 管理画面に独自JavaScriptを読み込ませる
 */
function myAdminScript() {
	wp_enqueue_script('my_admin_script', get_template_directory_uri(). '/js/admin.js', array('jquery'), '', true);
}
add_action('admin_enqueue_scripts', 'myAdminScript');

/**
 * 固定ページでのビジュアルエディタ使用を禁止する
 */
function disableVisualEditorInPage() {
	global $typenow;
	if ($typenow == 'page') {
		add_filter('user_can_richedit', 'disableVisualEditorFilter');
	}
}

function disableVisualEditorFilter() {
	return false;
}
add_action('load-post.php',     'disableVisualEditorInPage');
add_action('load-post-new.php', 'disableVisualEditorInPage');

/**
 * 固定ページ一覧に検索条件追加
 */
function addPageFilter() {
	if (get_current_screen()->post_type != 'page') { return; }

	$meta_key = 'language';
	$field = get_field_object($meta_key);
	$items = array_merge(array('' => 'すべての言語'), $field['choices']);
	$selected_value = filter_input(INPUT_GET, $meta_key);

	// プルダウンのHTML生成
	$output = '<p style="color: #618fff;font-weight: bold;float: left;margin: 5px 0 0 20px;">言語で絞込可能：</p>';
	$output .= '<select name="' . esc_attr($meta_key) . '">';
	foreach ($items as $value => $text) {
		$selected = selected($selected_value, $value, false);
		$output .= '<option value="' . esc_attr($value) . '"' . $selected . '>' . esc_html($text) . '</option>';
	}
	$output .= '</select>';

	echo $output;
}

add_action('restrict_manage_posts', 'addPageFilter');

/**
 * 固定ページ一覧に追加した検索条件をクエリに反映
 * @param WP_Query $query クエリオブジェクト
 */
function addQueryPageFilter($query) {
	if (filter_input(INPUT_GET, 'post_type') != 'page') { return; }
	if (!$query->is_main_query()) { return; }

	$meta_key = 'language';
	$meta_value = filter_input(INPUT_GET, $meta_key);

	// クエリの検索条件に追加
	if (strlen($meta_value)) {
		$meta_query = $query->get('meta_query');
		if (!is_array($meta_query)) { $meta_query = array(); }

		$meta_query[] = array(
			'key' => $meta_key,
			'value' => $meta_value
		);

		$query->set('meta_query', $meta_query);
	}
}

add_action('pre_get_posts', 'addQueryPageFilter');
