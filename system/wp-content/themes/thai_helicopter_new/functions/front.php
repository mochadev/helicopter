<?php
/**
 * フロント画面で使う共通処理
 */

initTheme();

/**
 * テーマを初期化する
 */
function initTheme() {
	$GLOBALS['lang'] = getLangByUri();
}

/**
 * URIから表示言語を取得する
 * @return string 表示言語
 */
function getLangByUri() {
	$uri_array = explode('/', $_SERVER['REQUEST_URI']);
	if (!isset($uri_array[1])) { return 'jp'; }

	switch ($uri_array[1]) {
		case 'th': return 'th';
		case 'en': return 'en';
		default:   return 'jp';
	}
}

/**
 * 内部的なリライトルールを変更
 */
function customRewrite() {
	$uri = $_SERVER['REQUEST_URI'];
	if (strpos($uri, '/jp/tour/') === 0) {
		wp_redirect(home_url(). mb_substr($uri, 3), 301);
	}
	if (strpos($uri, '/en/tour/') !== 0 && strpos($uri, '/th/tour/') !== 0) { return ; }
	$_SERVER['REQUEST_URI'] = str_replace(array('/en/', '/th/'), '/', $uri);
}
add_action('init', 'customRewrite', 0);

/**
 * 固定ページの場合、出力されるpタグ、brタグを出さない
 */
function pageExcludeAutoP() {
	if (is_page()) {
		remove_filter('the_excerpt', 'wpautop');
		remove_filter('the_content', 'wpautop');
	}
}
add_action('wp', 'pageExcludeAutoP');

/**
 * 指定した投稿の本文を表示する
 * @param array|int		$post_id	投稿ID
 * @param array|string	$post_type	投稿タイプ
 */
function showPost($post_id, $post_type='parts') {
	// 対象の投稿を取得
	$args = array(
		'p' => $post_id,
		'post_type' => $post_type
	);
	$query = new WP_Query($args);

	// 本文出力
	if ($query->have_posts()) {
		$query->the_post();
		the_content();
	}
	wp_reset_query();
}

/**
 * 問い合わせを表示
 */
function showPageContact() {
	switch ($GLOBALS['lang']) {
		case 'jp':
		default: $post_id = 29; break;
	}
	showPost($post_id);
}

/**
 * ページの#headのクラス名に変換する
 * @param string $key	元となる文字列
 * @param array $args	パラメータ
 * @return string		クラス名
 */
function toPageHeadClass($key, $args) {
	$post_name = str_replace('-'. $GLOBALS['lang'], '', $key);
	if ($args['has_slider']) {
		return 'slideBack';
	}
	return $post_name. 'Back';
}

/*
	アーカイブページで現在のカテゴリー・タグ・タームを取得する
*/
function getCurrentTerm() {
	if (is_category()) {
		$tax_slug = "category";
		$id = get_query_var('cat');
	} else if (is_tag()) {
		$tax_slug = "post_tag";
		$id = get_query_var('tag_id');
	} else if(is_tax()) {
		$tax_slug = get_query_var('taxonomy');
		$term_slug = get_query_var('term');
		$term = get_term_by("slug", $term_slug, $tax_slug);
		$id = $term->term_id;
	}

	return get_term($id, $tax_slug);
}

/**
 * ギャラリーのショートコードを展開する
 * @param array $atts	パラメータ
 * @return string		生成したHTML
 */
function shortcodeGallery($atts) {
	$atts = shortcode_atts(array('gallery_id' => '', 'class' => ''), $atts);
	extract($atts);

	if (strpos($class, 'pc') !== FALSE && isMobile()) {
		return;
	}

	$type = get_field('type', $gallery_id);

	// HTML生成
	$html = '<ul class="'. $type. '_slider '. $class. '">'. PHP_EOL;
	for ($i=1; $i<=20; $i++) {
		$html .= generateGalleryItemHtml($gallery_id, $type, $i);
	}
	$html .= '</ul>'. PHP_EOL;

	if ($type === 'charter') {
		$html .= '<ul class="'. $type. '_slider_nav '. $class. '">'. PHP_EOL;
		for ($i=1; $i<=20; $i++) {
			$html .= generateGalleryItemHtml($gallery_id, $type, $i, array('nav' => true));
		}
		$html .= '</ul>'. PHP_EOL;
	}

	return $html;
}
add_shortcode('gallery', 'shortcodeGallery');

/**
 * ギャラリーリストHTMLを生成する
 * @param int $gallery_id	ギャラリーID
 * @param string $type		ギャラリー種類
 * @param int $cnt			行数
 * @param int $args			オプションパラメータ
 * @return string			生成したHTML
 */
function generateGalleryItemHtml($gallery_id, $type, $cnt, array $args=array()) {
	// 画像ID取得
	$image_id = get_field('image'. $cnt, $gallery_id);
	if (empty($image_id)) { return; }

	// 画像URL取得
	$image_thumbnail_array = wp_get_attachment_image_src($image_id);
	$image_thumbnail = $image_thumbnail_array[0];
	$image_large_array = wp_get_attachment_image_src($image_id, 'large');
	$image_large = $image_large_array[0];
	$image_full_array = wp_get_attachment_image_src($image_id, 'full');
	$image_full = $image_full_array[0];

	// 追加フィールド情報取得
	$caption = get_field('caption'. $cnt, $gallery_id);
	$url = get_field('url'. $cnt, $gallery_id);
	$headline = get_field('headline'. $cnt, $gallery_id);
	$a_target = 'target="_blank"';
	if (empty($url)) {
		$url = $image_full;
		$a_target = '';
	}

	// ギャラリー種類毎にHTMLを生成
	switch ($type) {
		case 'sns':
			$html = <<<EOM
<li>
	<a href="{$url}" {$a_target}>
		<img src="{$image_full}">
		<div class="caption">{$caption}</div>
	</a>
</li>
EOM;
			break;
		case 'team':
			$id = sprintf('p%03d', $cnt);
			$html = <<<EOM
<li>
	<a href="javascript:void(0);" {$a_target}>
		<div class="col-team circle person001" id="{$id}" style="background-image: url({$image_large});">
			<input type="hidden" class="headline" value="{$headline}">
			<input type="hidden" class="caption" value="{$caption}">
			<input type="hidden" class="image" value="{$image_full}">
		</div>
	</a>
</li>
EOM;
			break;
		case 'head':
			$html = <<<EOM
<li>
	<img src="{$image_full}">
	<div class="caption">{$caption}</div>
</li>
EOM;
			break;
		case 'charter':
				if (isset($args['nav']) && $args['nav']) {
					$image = $image_thumbnail;
				} else {
					$image = $image_full;
				}
				$html = <<<EOM
	<li>
		<img src="{$image}">
		<div class="caption">{$caption}</div>
	</li>
EOM;
			break;
		default:
			$html = <<<EOM
<li>
	<a href="{$url}" {$a_target}>
		<img src="{$image_large}">
		<div class="caption">{$caption}</div>
	</a>
</li>
EOM;
			break;
	}
	return $html;
}

/**
 * ツアー名画像パスに変換する
 * @param WP_Post $post	投稿オブジェクト
 * @return string		画像パス
 */
function toTourNameImagePath(WP_Post $post) {
	$image_name = strtolower(trim($post->post_name));
	$image_name = str_replace(' ', '_', $image_name). '_txt.png';
	return '/images/'. $image_name;
}

/**
 * クエリーをmenu_order順にする
 * @param WP_Query $query クエリーオブジェクト
 */
function queryMenuOrder($query) {
	if (is_tax('tour_cat')) {
		$query->set('orderby', 'menu_order');
		$query->set('order', 'ASC');
	}
}
add_action('pre_get_posts', 'queryMenuOrder');

/**
 * パーマリンクを取得
 * @param object|int $post 投稿オブジェクト | 投稿ID
 * @return string パーマリンク
 */
function getPermalink($post) {
	$permalink = get_the_permalink($post);
	if ($GLOBALS['lang'] == 'jp') {
		return $permalink;
	} else {
		return str_replace(home_url(), home_url(). '/'. $GLOBALS['lang'], $permalink);
	}
}

/**
 * カスタムフィールドの値を取得（言語毎に取得するフィールドを切り替え）
 * @param string $field_name フィールド名
 * @param string $post_id 投稿ID
 * @return string カスタムフィールドの値文字列
 */
function theFieldLang($field_name, $post_id= false) {
	if ($GLOBALS['lang'] != 'jp') {
		$field_name .=  '_'. $GLOBALS['lang'];
	}
	the_field($field_name, $post_id);
}

/**
 * カスタムフィールドの値を取得（言語毎に取得するフィールドを切り替え）
 * @param string $field_name フィールド名
 * @param string $post_id 投稿ID
 * @return string カスタムフィールドの値文字列
 */
function getFieldLang($field_name, $post_id = false, $format_value = true) {
	if ($GLOBALS['lang'] != 'jp') {
		$field_name .= '_'. $GLOBALS['lang'];
	}
	return get_field($field_name, $post_id, $format_value);
}

/**
 * 本文を出力（言語毎に出力するフィールドを切り替え）
 */
function theContentLang($more_link_text = null, $strip_teaser = false) {
	if ($GLOBALS['lang'] == 'jp') {
		the_content($more_link_text, $strip_teaser);
	} else {
		echo apply_filters('the_content', getFieldLang('content'));
	}
}

function theContentLangText($more_link_text = null, $strip_teaser = false) {
	if ($GLOBALS['lang'] == 'jp') {
		the_content($more_link_text, $strip_teaser);
	} else {
		return apply_filters('the_content', getFieldLang('content'));
	}
}


function getDevice() {
	/* 端末のUAを取得 */
    $ua = $_SERVER['HTTP_USER_AGENT'];

    /* iPhone/iPod/Androidスマホが該当 */
    if ((strpos($ua, 'iPhone') !== false)
    || (strpos($ua, 'iPod') !== false)
    || (strpos($ua, 'Android') !== false)
    && (strpos($ua, 'Mobile') !== false)) {
        return 'sp';
    }

    /* iPhone/iPad/iPod/Androidスマホ/Androidタブレットが該当 */
    if ((strpos($ua, 'iPhone') !== false)
    || (strpos($ua, 'iPod') !== false)
    || (strpos($ua, 'iPad') !== false)
    || (strpos($ua, 'Android') !== false)) {
        return 'tab';
    }
}

function isMobile() {
	$device = getDevice();
	return $device === 'sp';
}
