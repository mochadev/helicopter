<?php
/**
 * 多言語テキストを設定・出力するクラス
 */
 class LangText {
     /**
      * 言語毎の原稿配列
      */
     private static $text_list = array(
         'TOUR_SINGLE_01' => array(
             'jp' => '価格', 'en' => 'Price', 'th' => 'ราคา',
         ),
         'TOUR_SINGLE_02' => array(
             'jp' => '出発地',  'en' => 'Place of Departure', 'th' => 'จุดเริ่มต้น',
         ),
         'TOUR_SINGLE_03' => array(
             'jp' => '目的地',  'en' => 'Destination', 'th' => 'ปลายทาง',
         ),
         'TOUR_SINGLE_04' => array(
             'jp' => '飛行時間(片道)', 'en' => 'Flight Hours', 'th' => 'ระยะเวลาบิน',
         ),
				 'TOUR_SINGLE_05' => array(
					 'jp' => '(往復)', 'en' => '(Round Trip)', 'th' => '(ป-กลับ)',
				 ),
				 'TOUR_SINGLE_06' => array(
					 'jp' => '(4回)', 'en' => '(4 Times)', 'th' => '(4 รอบ)',
				 ),
				 'TOUR_SINGLE_07' => array(
					 'jp' => '(4h以降3,000THB/1h)', 'en' => '(After 4 hour)', 'th' => '(หลัง 4 ชั่วโมง)',
				 ),
         'min' => array(
					 'jp' => '分', 'en' => 'mins', 'th' => 'นาที',
				 )
     );
     /**
      * カスタムフィールドの原稿配列
      */
     private static $custom_field_text_list = array(
         'flight_time_unit' => array(
             'minute' => array(
                 'jp' => '分', 'en' => 'mins', 'th' => 'นาท'
             ),
             'hour' => array(
                 'jp' => '時間', 'en' => 'hour', 'th' => 'hour'
             ),
         )
     );
	/**
	 * テキストを出力
	 * @param string $key 出力するキー
	 */
	public static function output($key) {
		if (!isset(self::$text_list[$key][$GLOBALS['lang']])) { return; }
		echo self::$text_list[$key][$GLOBALS['lang']];
	}

	//こっちは文言だけ返すメソッドです
	public static function outputText($key){
		if (!isset(self::$text_list[$key][$GLOBALS['lang']])) { return; }
		return self::$text_list[$key][$GLOBALS['lang']];
	}

	/**
	 * カスタムフィールドの値を取得（言語毎に文言を切り替え）
	 * @param string $field_name フィールド名
	 * @param string $post_id 投稿ID
	 * @return string カスタムフィールドの値文字列
	 */
	public static function getField($field_name, $post_id = false) {
		$field_object = get_field_object($field_name, $post_id);
		$value = $field_object['value'];
		if (isset(self::$custom_field_text_list[$field_name][$value][$GLOBALS['lang']])) {
			return self::$custom_field_text_list[$field_name][$value][$GLOBALS['lang']];
		}
	}

	/**
	 * カスタムフィールドの値を出力（言語毎に文言を切り替え）
	 * @param string $field_name フィールド名
	 * @param string $post_id 投稿ID
	 * @return string カスタムフィールドの値文字列
	 */
	public static function theField($field_name, $post_id = false) {
		echo self::getField($field_name, $post_id);
	}
}
