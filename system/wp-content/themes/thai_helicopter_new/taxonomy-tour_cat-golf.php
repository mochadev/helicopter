<?php
/**
 * ツアー ゴルフ一覧表示
 */

global $term;
$term = getCurrentTerm();

$langText = new LangText();
$package001 = $langText->outputText('TOUR_SINGLE_05');
$package002 = $langText->outputText('TOUR_SINGLE_06');
$minute = $langText->outputText('min');
get_header(); ?>

<?php get_template_part('template/tour_cat_head'); ?>
    <div class="golfService">
        <h2 id="serviceTitle">Service</h2>
        <p class="intro">
            <?php theFieldLang('catch_copy', 'tour_cat_'. $term->term_id); ?>
        </p>
        <div class="layer">
    </div>
    </div>
    <div class="golfDestination">
    <?php
    if (have_posts()) :
            $cnt = 0;
            while (have_posts()) :
                    the_post();
                    $cnt++;
                    // アイキャッチ画像を取得
                    $image_url = get_bloginfo('template_directory'). '/images/thumbnail.png';
                    if (has_post_thumbnail()) {
                            $image_id = get_post_thumbnail_id();
                            $image_src = wp_get_attachment_image_src($image_id, true);
                            if (isset($image_src[0])) {
                                    $image_url = $image_src[0];
                            }
                    }
    ?>
            <div class="golfFlightDescription">
                    <ul>
                            <p id="golfTitle"><?php echo get_field('place'); ?></p>
                            <li><p><?php echo get_field('time').$minute; ?></p></li>
                            <li><p style="margin-bottom:5px;"><?php echo get_field('price'); ?></p>
                            <li class="golfCot"><?php echo theContentLangText(); ?></li>
                    </ul>
                    <span class="golfFlightImage">
                            <img src="<?php echo $image_url; ?>" class="back">
                    </span>
            </div>
    <?php
            endwhile;
    else:
    ?>
            <p>Comming soon!</p>
    <?php
    endif;
    ?>
</div>
<div class="golfSample">
        <h2 id="golfSampleTitle">Sample Schedule</h2>
        <div class="scheduleContent">
                <table>
                        <tbody>
                            <tr>
                                <td id="time">??:??</td>
                                <td id="content">ここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきます</td>
                            </tr>
                            <tr>
                                <td id="time">??:??</td>
                                <td id="content">ここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきます</td>
                            </tr>
                            <tr>
                                <td id="time">??:??</td>
                                <td id="content">ここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきます</td>
                            </tr>
                        </tbody>
                </table>
        </div>
        <div id="scheduleImage">
            <img src="<?php echo $image_url; ?>" />
            <img id="scheduleBelowImage" src="<?php echo $image_url; ?>" /></div>
</div>
<div class="golfDeparture">
        <h2>Departure</h2>
        <ul>

                <li>
                        <p id="departureTitle">DON MUEANG AIRPORT</p>
                        <p id="departureDesc">ここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきます</p>
                        <p id="departurePrice">PRICE</p>
                        <p id="departureAccess">ACCESS</p>
                        <div class="ggmap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15491.041906078424!2d100.6041987!3d13.9132602!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf0100b33d0bffa0!2sDon+Mueang+International+Airport!5e0!3m2!1sen!2sjp!4v1527059379593" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                </li>
                <li>
                        <p id="departureTitle">BHIRAJ TOWER</p>
                        <p id="departureDesc">ここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきます</p>
                        <p id="departurePrice">PRICE</p>
                        <p id="departureAccess">ACCESS</p>
                        <div class="ggmap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.784881257647!2d100.56761925094429!3d13.731469901415254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f019482119b%3A0x8d5017500d5909ea!2sBhiraj+Tower+at+EmQuartier!5e0!3m2!1sen!2sjp!4v1527059581773" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                </li>
                <li>
                        <p id="departureTitle">PENINSULA HOTEL</p>
                        <p id="departureDesc">ここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきます</p>
                        <p id="departurePrice">PRICE</p>
                        <p id="departureAccess">ACCESS</p>
                        <div class="ggmap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.9242275199745!2d100.50892095094433!3d13.723037201608058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298c339253793%3A0x60af949acb412925!2sThe+Peninsula+Spa!5e0!3m2!1sen!2sjp!4v1527059634691" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                </li>
                <li>
                        <p id="departureTitle">SKYWALK CONDOMINIUM</p>
                        <p id="departureDesc">ここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきますここに説明文がきます</p>
                        <p id="departurePrice">PRICE</p>
                        <p id="departureAccess">ACCESS</p>
                        <div class="ggmap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3876.06932258452!2d100.59128395094419!3d13.714251201808898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29fbecfadd831%3A0xeeaac2fea7f09168!2sSky+Walk+Residences!5e0!3m2!1sen!2sjp!4v1527059704812" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                </li>
        </ul>
</div>
<?php get_footer();
